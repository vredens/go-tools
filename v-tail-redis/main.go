package main

import (
	"flag"
	"fmt"
	"log"

	"github.com/go-redis/redis"
)

var defaults = struct {
	host string
	port int
	db   int
}{
	host: "localhost",
	port: 6379,
	db:   0,
}

func main() {
	// direct connect parameters
	var host = flag.String("host", defaults.host, "redis host address")
	var port = flag.Int("port", defaults.port, "redis port")
	var db = flag.Int("db", defaults.db, "redis database number")
	var pattern = flag.String("pattern", "logs", "redis channel pattern to subscribe to")

	log.SetFlags(log.Ltime)

	flag.Parse()

	client := redis.NewClient(&redis.Options{
		Addr:     fmt.Sprintf("%s:%d", *host, *port),
		Password: "",
		DB:       *db,
	})

	pubsub := client.PSubscribe(*pattern)

	msg, err := pubsub.ReceiveMessage()
	for err == nil && msg != nil {
		fmt.Printf("%s\n", msg.Payload)
		msg, err = pubsub.ReceiveMessage()
	}

	if err != nil {
		fmt.Printf("error: %s\n", err)
	}
}
