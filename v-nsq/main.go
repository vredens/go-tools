package main

import (
	"flag"
	"fmt"

	"time"
)

// StatsMessage ...
type StatsMessage struct {
	StatusCode int              `json:"status_code"`
	Data       StatsDataMessage `json:"data"`
}

// StatsDataMessage ...
type StatsDataMessage struct {
	Health    string         `json:"health"`
	Topics    []TopicMessage `json:"topics"`
	StartTime int64          `json:"start_time"`
}

// TopicMessage ...
type TopicMessage struct {
	Name         string           `json:"topic_name"`
	Channels     []ChannelMessage `json:"channels"`
	Depth        int              `json:"depth"`
	MessageCount int              `json:"message_count"`
}

// ChannelMessage ...
type ChannelMessage struct {
	Name         string          `json:"channel_name"`
	Depth        int             `json:"depth"`
	MessageCount int             `json:"message_count"`
	RequeueCount int             `json:"requeue_count"`
	TimeoutCount int             `json:"timeout_count"`
	Clients      []ClientMessage `json:"clients"`
}

// ClientMessage ...
type ClientMessage struct {
	Name          string `json:"name"`
	RemoteAddress string `json:"remote_address"`
	ConnectTime   int64  `json:"connect_ts"`
}

var defaults = struct {
	host string
	port int
}{
	host: "localhost",
	port: 4151,
}

func main() {
	// direct connect parameters
	var host = flag.String("host", defaults.host, "the NSQ server hostname or IP address")
	var port = flag.Int("port", defaults.port, "the NSQ server HTTP port")
	var dashboard = flag.Int("dash", 1, "enables dashboard mode")
	var filterTopic = flag.String("filter-topic", "", "filter topics matching the regex provided")
	var filterChannel = flag.String("filter-channel", "", "filter channels matching the regex provided")
	// var autoSnapshot = flag.Bool("snapshot", false, "enable auto-snapshot after each report")

	flag.Parse()

	var opts = &NSQStatsOptions{
		topicFilter:   *filterTopic,
		channelFilter: *filterChannel,
	}

	switch *dashboard {
	case 0:
		printStats(*host, *port, opts)
	case 1:
		launchDashboard(*host, *port, opts)
	}
}

func launchDashboard(host string, port int, opts *NSQStatsOptions) {
	stats := NewNSQStatsCollector(host, port, opts)
	dashboard := NewDashboardUI(stats)

	dashboard.Start()
}

func printStats(host string, port int, opts *NSQStatsOptions) {
	stats := NewNSQStatsCollector(host, port, &NSQStatsOptions{
		showEmpty: false,
		topic:     "",
	})
	data := stats.NewReport()

	fmt.Printf("| %-10s | %-10s | %-10s | %-10s | %-10s | %s\n", "Conns", "Depth", "MCnt", "RCnt", "TCnt", "Topic -> Channel (connect time)")
	for _, row := range data {
		fmt.Print("| ")
		for _, col := range row {
			fmt.Printf("%-10s | ", col)
		}
		fmt.Printf("\n")
	}
}

func analyseTopicChannelsV2(topic string, channels []ChannelMessage) {
	if len(channels) == 0 {
		return
	}
	// fmt.Printf("| %-10s | %-10s | %-10s | %-10s | %-10s | %s\n", "Conns", "Depth", "MCnt", "RCnt", "TCnt", topic)
	for _, channel := range channels {
		var minTime time.Time
		for _, client := range channel.Clients {
			loladaTime := time.Unix(client.ConnectTime, 0)
			if minTime.IsZero() || loladaTime.Before(minTime) {
				minTime = loladaTime
			}
		}
		fmt.Printf("| %10d | %10d | %10d | %10d | %10d | %50s -> %s (%s)\n",
			len(channel.Clients),
			channel.Depth,
			channel.MessageCount,
			channel.RequeueCount,
			channel.TimeoutCount,
			topic,
			channel.Name,
			minTime,
		)
	}
}
