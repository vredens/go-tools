package main

// NSQTopicStats ...
type NSQTopicStats struct {
	channels     map[string]*NSQChannelStats
	channelIndex []string
	stats        *NSQStatsHistory
}

// NSQStatsHistory ...
type NSQStatsHistory struct {
	Current  *NSQStats
	Snapshot *NSQStats
}

// NSQChannelStats ...
type NSQChannelStats struct {
	Current  *NSQStats
	Snapshot *NSQStats
}

// NSQStats ...
type NSQStats struct {
	Counter      int
	Depth        int
	MessageCount int
	RequeueCount int
	TimeoutCount int
}
