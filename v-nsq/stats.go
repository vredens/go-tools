package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"regexp"

	"sort"

	"gitlab.com/mandalore/go-app/app"
)

// NSQStatsOptions ...
type NSQStatsOptions struct {
	topic         string
	showEmpty     bool
	topicFilter   string
	channelFilter string
}

// NSQStatsCollector ...
type NSQStatsCollector struct {
	endpoint   string
	options    *NSQStatsOptions
	topics     map[string]*NSQTopicStats
	topicIndex []string
	lastReport [][]string
	topicRE    *regexp.Regexp
	channelRE  *regexp.Regexp
	gw         *http.Client
}

// NewNSQStatsCollector creates a new instance of a NSQStatsCollector
func NewNSQStatsCollector(host string, port int, options *NSQStatsOptions) *NSQStatsCollector {
	stats := &NSQStatsCollector{
		endpoint:   fmt.Sprintf("http://%s:%d", host, port),
		options:    options,
		topics:     make(map[string]*NSQTopicStats),
		topicIndex: make([]string, 0),
		gw:         &http.Client{},
	}

	if stats.options == nil {
		stats.options = &NSQStatsOptions{}
	}

	if stats.options.channelFilter != "" {
		stats.channelRE = regexp.MustCompile(stats.options.channelFilter)
	}

	if stats.options.topicFilter != "" {
		stats.topicRE = regexp.MustCompile(stats.options.topicFilter)
	}

	return stats
}

func (stats *NSQStatsCollector) collect(snapshot bool) {
	res, err := stats.gw.Get(fmt.Sprintf("%s/stats?format=json", stats.endpoint))
	if err != nil {
		app.Logger.Errorf("error fetching stats; %s", app.StringifyError(err))
	}
	data, err := ioutil.ReadAll(res.Body)
	if err != nil {
		app.Logger.Errorf("error reading response body; %s", app.StringifyError(err))
	}
	defer res.Body.Close()

	var statsData *StatsDataMessage

	// NSQ version <1.0
	msg := &StatsMessage{}
	if err := json.Unmarshal(data, msg); err != nil {
		app.Logger.Errorf("error unmarshalling; %s", app.StringifyError(err))
	}
	if msg.StatusCode == 0 {
		statsData = &StatsDataMessage{}
		if err := json.Unmarshal(data, statsData); err != nil {
			app.Logger.Errorf("error unmarshalling; %s", app.StringifyError(err))
		}
	} else {
		statsData = &msg.Data
	}
	topics := statsData.Topics

	for _, topic := range topics {
		if stats.topicRE != nil && !stats.topicRE.MatchString(topic.Name) {
			continue
		}
		if topic.MessageCount == 0 {
			if stats.topics[topic.Name] != nil {
				delete(stats.topics, topic.Name)
			}
			continue
		}

		if stats.topics[topic.Name] == nil {
			stats.topics[topic.Name] = &NSQTopicStats{
				channels:     make(map[string]*NSQChannelStats),
				channelIndex: make([]string, 0, len(topic.Channels)),
				stats:        &NSQStatsHistory{},
			}
			stats.topicIndex = append(stats.topicIndex, topic.Name)
		}

		if snapshot {
			stats.topics[topic.Name].stats.Snapshot = &NSQStats{
				MessageCount: topic.MessageCount,
				Depth:        topic.Depth,
			}
		} else {
			stats.topics[topic.Name].stats.Current = &NSQStats{
				MessageCount: topic.MessageCount,
				Depth:        topic.Depth,
			}
		}

		for _, channel := range topic.Channels {
			if stats.channelRE != nil && !stats.channelRE.MatchString(channel.Name) {
				continue
			}
			if stats.topics[topic.Name].channels[channel.Name] == nil {
				stats.topics[topic.Name].channels[channel.Name] = &NSQChannelStats{}
				stats.topics[topic.Name].channelIndex = append(stats.topics[topic.Name].channelIndex, channel.Name)
			}

			tmp := &NSQStats{
				Counter:      len(channel.Clients),
				Depth:        channel.Depth,
				MessageCount: channel.MessageCount,
				RequeueCount: channel.RequeueCount,
				TimeoutCount: channel.TimeoutCount,
			}

			if snapshot {
				stats.topics[topic.Name].channels[channel.Name].Snapshot = tmp
			} else {
				stats.topics[topic.Name].channels[channel.Name].Current = tmp
			}
		}
	}

	stats.lastReport = stats.generateReport()
}

// GetReport returns the last generated report.
func (stats *NSQStatsCollector) generateReport() [][]string {
	overview := make([][]string, 0, 0)

	overview = append(overview, []string{"topic", "channel", "(±conns)", "conns", "(±Depth)", "Depth", "(±MCnt)", "MCnt", "(±RCnt)", "RCnt", "(±TCnt)", "TCnt"})

	sort.Strings(stats.topicIndex)
	for _, topicName := range stats.topicIndex {
		topicStats := stats.topics[topicName]
		sort.Strings(topicStats.channelIndex)
		if len(topicStats.channelIndex) == 0 {
			if topicStats.stats.Snapshot != nil {
				overview = append(overview, []string{
					topicName,
					"N/A",
					fmt.Sprintf("(%+6d)", 0),
					fmt.Sprintf("%6d", 0),
					fmt.Sprintf("(%+6d)", topicStats.stats.Current.Depth-topicStats.stats.Snapshot.Depth),
					fmt.Sprintf("%6d", topicStats.stats.Current.Depth),
					fmt.Sprintf("(%+6d)", topicStats.stats.Current.MessageCount-topicStats.stats.Snapshot.MessageCount),
					fmt.Sprintf("%6d", topicStats.stats.Current.MessageCount),
					fmt.Sprintf("(%+6d)", 0),
					fmt.Sprintf("%6d", 0),
					fmt.Sprintf("(%+6d)", 0),
					fmt.Sprintf("%6d", 0),
				})
			} else {
				overview = append(overview, []string{
					topicName,
					"N/A",
					fmt.Sprintf("(%+6d)", 0),
					fmt.Sprintf("%6d", 0),
					fmt.Sprintf("(%+6d)", 0),
					fmt.Sprintf("%6d", topicStats.stats.Current.Depth),
					fmt.Sprintf("(%+6d)", 0),
					fmt.Sprintf("%6d", topicStats.stats.Current.MessageCount),
					fmt.Sprintf("(%+6d)", 0),
					fmt.Sprintf("%6d", 0),
					fmt.Sprintf("(%+6d)", 0),
					fmt.Sprintf("%6d", 0),
				})
			}
		} else {
			for _, channelName := range topicStats.channelIndex {
				channelStats := topicStats.channels[channelName]
				if channelStats.Current != nil {
					if channelStats.Snapshot != nil {
						overview = append(overview, []string{
							topicName,
							channelName,
							fmt.Sprintf("(%+6d)", channelStats.Current.Counter-channelStats.Snapshot.Counter),
							fmt.Sprintf("%6d", channelStats.Current.Counter),
							fmt.Sprintf("(%+6d)", channelStats.Current.Depth-channelStats.Snapshot.Depth),
							fmt.Sprintf("%6d", channelStats.Current.Depth),
							fmt.Sprintf("(%+6d)", channelStats.Current.MessageCount-channelStats.Snapshot.MessageCount),
							fmt.Sprintf("%6d", channelStats.Current.MessageCount),
							fmt.Sprintf("(%+6d)", channelStats.Current.RequeueCount-channelStats.Snapshot.RequeueCount),
							fmt.Sprintf("%6d", channelStats.Current.RequeueCount),
							fmt.Sprintf("(%+6d)", channelStats.Current.TimeoutCount-channelStats.Snapshot.TimeoutCount),
							fmt.Sprintf("%6d", channelStats.Current.TimeoutCount),
						})
					} else {
						overview = append(overview, []string{
							topicName,
							channelName,
							fmt.Sprintf("(%+6d)", 0),
							fmt.Sprintf("%6d", channelStats.Current.Counter),
							fmt.Sprintf("(%+6d)", 0),
							fmt.Sprintf("%6d", channelStats.Current.Depth),
							fmt.Sprintf("(%+6d)", 0),
							fmt.Sprintf("%6d", channelStats.Current.MessageCount),
							fmt.Sprintf("(%+6d)", 0),
							fmt.Sprintf("%6d", channelStats.Current.RequeueCount),
							fmt.Sprintf("(%+6d)", 0),
							fmt.Sprintf("%6d", channelStats.Current.TimeoutCount),
						})
					}
				}
			}
		}
	}

	return overview
}

// ReportFields returns the list of field names of the report line, i.e., the report header. Note that this is already included in the report as the first entry.
func (stats *NSQStatsCollector) ReportFields() []string {
	return []string{"topic", "channel", "(±conns)", "conns", "(±Depth)", "Depth", "(±MCnt)", "MCnt", "(±RCnt)", "RCnt", "(±TCnt)", "TCnt"}
}

// Snapshot takes a snapshot of current statistics for comparison on next report.
func (stats *NSQStatsCollector) Snapshot() {
	sort.Strings(stats.topicIndex)
	for _, topicStats := range stats.topics {
		for _, channelStats := range topicStats.channels {
			channelStats.Snapshot = channelStats.Current
		}
	}
}

// GetReport returns the last generated report or a new one if no report has been generated so far.
func (stats *NSQStatsCollector) GetReport() [][]string {
	if stats.lastReport != nil {
		return stats.lastReport
	}

	return stats.NewReport()
}

// NewReport generates a new report.
func (stats *NSQStatsCollector) NewReport() [][]string {
	stats.collect(false)

	return stats.lastReport
}
