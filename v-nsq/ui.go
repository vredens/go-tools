package main

import (
	"fmt"
	"time"

	ui "github.com/gizak/termui"
)

// Selector ...
type Selector struct {
	cur int
	min int
	max int
}

// NewSelector ...
func NewSelector(min, max int) *Selector {
	return &Selector{
		max: max,
		min: min,
		cur: 1,
	}
}

// Update ...
func (s *Selector) Update(min, max int) {
	s.min = min
	s.max = max
	if s.max == s.min {
		s.cur = s.max
	} else if s.max < s.min {
		panic("devpoo")
	}
	if s.cur > s.max {
		s.cur = s.max
	} else if s.cur < s.min {
		s.cur = s.min
	}
}

// Up ...
func (s *Selector) Up() int {
	if s.cur <= s.min {
		s.cur = s.max
	} else {
		s.cur--
	}

	return s.cur
}

// Down ...
func (s *Selector) Down() int {
	if s.cur >= s.max {
		s.cur = s.min
	} else {
		s.cur++
	}

	return s.cur
}

// Current ...
func (s *Selector) Current() int {
	return s.cur
}

// DashboardUI ...
type DashboardUI struct {
	table       *ui.Table
	statusLeft  *ui.Par
	statusRight *ui.Par
	sel         *Selector
	stats       *NSQStatsCollector
	report      [][]string
}

func NewDashboardUI(stats *NSQStatsCollector) *DashboardUI {
	return &DashboardUI{
		stats: stats,
	}
}

func (dashboard *DashboardUI) getReport() [][]string {
	data := dashboard.stats.GetReport()

	if len(data) < 1 {
		data = make([][]string, 2)
		data[0] = dashboard.stats.ReportFields()
		data[1] = make([]string, len(data[0]))
		for i := 0; i < len(data); i++ {
			data[1][i] = "n/a"
		}
	}

	dashboard.sel = NewSelector(1, len(data)-1)

	return data
}

func (dashboard *DashboardUI) createTable() *ui.Table {
	data := dashboard.getReport()

	table := ui.NewTable()
	table.Rows = data
	table.FgColor = ui.ColorWhite
	table.BgColor = ui.ColorDefault
	table.TextAlign = ui.AlignRight
	table.Separator = false
	table.PaddingTop = 0
	table.Analysis()
	table.SetSize()
	table.BgColors[0] = ui.ColorYellow
	table.FgColors[0] = ui.ColorBlack

	if len(data) > 1 {
		table.BgColors[dashboard.sel.Current()] = ui.ColorRed
	}

	table.Y = 10
	table.X = 0
	table.Border = true
	table.BorderLabel = "NSQ Topics"

	dashboard.table = table

	return dashboard.table
}

// Start ...
func (dashboard *DashboardUI) Start() {
	data := dashboard.stats.NewReport()

	if err := ui.Init(); err != nil {
		panic(err)
	}
	defer ui.Close()

	var counter uint
	var timer uint = 10
	var snapshotTime time.Time
	lastUpdateTime := time.Now()

	dashboard.createTable()

	statusRight := ui.NewPar(fmt.Sprintf("updating every %d seconds", timer))
	statusRight.Height = 3
	statusRight.BorderLabel = "Status"

	statusLeft := ui.NewPar(uiRenderLastUpdate(lastUpdateTime, snapshotTime))
	statusLeft.Height = 3
	statusLeft.BorderLabel = "Info"

	ui.Body.AddRows(ui.NewRow(
		ui.NewCol(6, 0, statusLeft),
		ui.NewCol(6, 0, statusRight),
	), ui.NewRow(
		ui.NewCol(12, 0, dashboard.table),
	))

	ui.Body.Align()
	ui.Render(ui.Body)

	ui.Handle("/sys/kbd/C-q", func(ui.Event) {
		ui.StopLoop()
	})
	ui.Handle("/sys/kbd/C-s", func(ui.Event) {
		dashboard.stats.Snapshot()
		snapshotTime = lastUpdateTime
		statusLeft.Text = uiRenderLastUpdate(lastUpdateTime, snapshotTime)
		ui.Render(statusLeft)
	})

	ui.Handle("/sys/kbd/<up>", func(ui.Event) {
		dashboard.table.BgColors[dashboard.sel.Current()] = ui.ColorDefault
		dashboard.table.BgColors[dashboard.sel.Up()] = ui.ColorRed
		ui.Render(ui.Body)
	})
	ui.Handle("/sys/kbd/<down>", func(ui.Event) {
		dashboard.table.BgColors[dashboard.sel.Current()] = ui.ColorDefault
		dashboard.table.BgColors[dashboard.sel.Down()] = ui.ColorRed
		ui.Render(ui.Body)
	})
	ui.Handle("/sys/kbd/<left>", func(ui.Event) {
		if timer > 5 {
			timer--
		}
		statusRight.Text = fmt.Sprintf("updating every %d seconds", timer)
		ui.Render(statusRight)
	})
	ui.Handle("/sys/kbd/<right>", func(ui.Event) {
		if timer < 60 {
			timer++
		}
		statusRight.Text = fmt.Sprintf("updating every %d seconds", timer)
		ui.Render(statusRight)
	})
	ui.Handle("/timer/1s", func(e ui.Event) {
		// t := e.Data.(ui.EvtTimer)
		counter++
		if counter >= timer {
			dashboard.table.Rows = dashboard.stats.NewReport()
			lastUpdateTime = time.Now()

			dashboard.sel.Update(1, len(data)-1)
			if len(dashboard.table.BgColors) > 1 {
				dashboard.table.BgColors[dashboard.sel.Current()] = ui.ColorRed
			}
			ui.Render(dashboard.table)

			statusLeft.Text = uiRenderLastUpdate(lastUpdateTime, snapshotTime)
			ui.Render(statusLeft)

			counter = 0
		}
	})

	ui.Handle("/sys/wnd/resize", func(e ui.Event) {
		ui.Body.Width = ui.TermWidth()
		ui.Body.Align()
		ui.Clear()
		ui.Render(ui.Body)
	})

	ui.Loop()
}

func uiRenderLastUpdate(now, snapshot time.Time) string {
	if snapshot.IsZero() {
		return fmt.Sprintf("at %s", now.String())
	}
	elapsed := now.Sub(snapshot)
	return fmt.Sprintf("at %s, comparing with the previous %.0f seconds", now.String(), elapsed.Seconds())
}
