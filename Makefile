.PHONY: tests

setup:
	glide install

start:
	docker-compose up -d psql mysql
	sleep 10

stop:
	docker-compose down -v

test: test-v-sql-run-psql test-v-sql-run-mysql

test-v-sql-run-psql:
	go run ./v-sql-run/main.go -type postgres -host localhost -user postgres -pass postgres -dbname postgres tests/v-sql-run-psql/

test-v-sql-run-mysql:
	go run ./v-sql-run/main.go -type mysql -host localhost -user testu -pass testp -dbname testdb tests/v-sql-run-mysql/

run-v-nsq:
	go run ./v-nsq/*go

install:
	mkdir -p .build
	mkdir -p .dist
	mkdir -p ~/bin
	go build -o .dist/v-nsq -pkgdir .build ./v-nsq/
	go build -o .dist/v-sql-run -pkgdir .build ./v-sql-run/
	go build -o .dist/v-tail-redis -pkgdir .build ./v-tail-redis/
	cp .dist/* ~/bin/
