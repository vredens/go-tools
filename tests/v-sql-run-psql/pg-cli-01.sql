CREATE SCHEMA vredens;

SET search_path TO vredens, public;

CREATE TABLE vredens.tmp (
    id  SMALLSERIAL NOT NULL,
    name       TEXT,
    PRIMARY KEY(id)
);

INSERT INTO vredens.tmp VALUES(1,'root');
INSERT INTO vredens.tmp VALUES(0,'vredens');
