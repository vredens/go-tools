package main

import (
	"database/sql"
	"flag"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"regexp"
	"sort"

	"io/ioutil"

	_ "github.com/go-sql-driver/mysql" // mysql sql driver
	_ "github.com/lib/pq"              // postgreSQL driver
)

var defaults = struct {
	dbType string
	dbHost string
	dbPort int
	dbUser string
	dbPass string
	dbName string
}{
	dbType: "postgres",
	dbUser: "postgres",
	dbPass: "postgres",
	dbName: "postgres",
}

func main() {
	// direct connect parameters
	var dbType = flag.String("type", defaults.dbType, "the database type, can be one of: postgres, mysql")
	var dbHost = flag.String("host", defaults.dbHost, "the database server hostname or IP address")
	var dbPort = flag.Int("port", defaults.dbPort, "database server port")
	var dbUser = flag.String("user", defaults.dbUser, "the database user")
	var dbPass = flag.String("pass", defaults.dbPass, "the database user's password")
	var dbName = flag.String("dbname", defaults.dbName, "the database name to connect to")
	var connString string

	// TODO
	// var connName = flag.String("db", "", "database name as per ./badmin.json configuration file.")

	log.SetFlags(log.Ltime)

	flag.Parse()

	if *dbPort == 0 {
		switch *dbType {
		case "postgres":
			*dbPort = 5432
		case "mysql":
			*dbPort = 3306
		}
	}

	switch *dbType {
	case "mysql":
		connString = fmt.Sprintf("%s:%s@tcp(%s:%d)/%s", *dbUser, *dbPass, *dbHost, *dbPort, *dbName)
	case "postgres":
		connString = fmt.Sprintf("%s://%s:%s@%s:%d/%s?sslmode=disable", *dbType, *dbUser, *dbPass, *dbHost, *dbPort, *dbName)
	default:
		log.Fatal("bad database type")
	}

	// if *connName != "" {
	// 	log.Fatal("not implemented")
	// }

	if connString == "" {
		log.Fatal("nothing to connect to")
	}

	fmt.Printf("connection string: %s\n", connString)

	conn, err := getConnection(*dbType, connString)
	if err != nil {
		log.Fatal(err)
	}

	b := &batcher{
		conn: conn,
	}

	for _, arg := range flag.Args() {
		if err = b.process(arg); err != nil {
			log.Fatalf("error processing [%s]; %s", arg, err.Error())
		}
	}

}

func getConnection(dbType, endpoint string) (*sql.DB, error) {
	conn, err := sql.Open(dbType, endpoint)
	conn.SetMaxIdleConns(1)
	conn.SetMaxOpenConns(2)

	if err != nil {
		return nil, err
	}

	if err = conn.Ping(); err != nil {
		return nil, err
	}

	return conn, err
}

type batcher struct {
	conn *sql.DB
}

func (b *batcher) process(path string) error {
	fi, err := os.Stat(path)
	if err != nil {
		return err
	}
	mode := fi.Mode()
	if mode.IsDir() {
		return b.processFolder(path)
	}

	return b.processFile(path)
}

func (b *batcher) processFile(path string) error {
	log.Printf("processing file [path:%s]", path)

	//
	data, err := ioutil.ReadFile(path)
	if err != nil {
		return err
	}

	if _, err = b.conn.Exec(string(data)); err != nil {
		return err
	}

	return nil
}

func (b *batcher) processFolder(path string) error {
	isSQL := regexp.MustCompile("\\.sql$")
	filesToProcess := make(sort.StringSlice, 0, 100)

	if err := filepath.Walk(path, func(path string, info os.FileInfo, err error) error {
		if err == nil && isSQL.MatchString(path) {
			filesToProcess = append(filesToProcess, path)
		}
		return nil
	}); err != nil {
		return err
	}

	filesToProcess.Sort()

	for i := 0; i < filesToProcess.Len(); i++ {
		if err := b.processFile(filesToProcess[i]); err != nil {
			return err
		}
	}

	return nil
}
