# Go Tools for daily operations and development

All tools are prefixed with a `v-` for making it easy for tab-completion on the terminal. The `v` stands for... **v**ery useful, but you probably thought it was Vredens and that's OK.

# Requirements

## for installing

* `go` (at least 1.8)

## for development

* `go` (at least 1.8)
* `glide`
* `docker` (at least 1.13)
* `docker-compose` (at least 1.13)


# The Tools

## v-sql-run

A tool for running multiple SQL script files (must have the `.sql` sufix) against a SQL database (check out the supported database types). Also supports scanning an entire directory and all subdirectories for SQL files and run them sorted by full path name.

Supported database types are

* PostgreSQL
* MySQL

Install it using

```
go get gitlab.com/vredens/go-tools/v-sql-run
```

Command syntax: `v-sql-run [OPTIONS] [<path> ...]`

Options

  * `-h` a short help which should provide more up to date option support.
  * `-port <value>`
	* `-user <username>`
	* `-pass <password>`
	* `-type <database type>`, can be one of `mysql` or `postgres`, defaults to `postgres`
	* `-dbname <database name>`

Example:

```
v-sql-run -type postgres -user postgresql -pass postgres -host localhost -port 5432 -dbname postgres ./tests/v-pg-run/ ./tests/v-pg-run/file.sql ./more/sql/files/here/
```

## v-nsq

A console dashboard for NSQ.

Install it using

```
go get gitlab.com/vredens/go-tools/v-nsq
```

Command syntax: `v-nsq [OPTIONS] [<path> ...]`

Options

  * `-h` a short help which should provide more up to date option support.
	* `-host <nsq host>`
  * `-port <value>`

Example:

```
v-nsq -host localhost -port 4151
```

Commands:

- `up/down`: navigate through the list
- `left/right`: decrease/increase refresh interval, minimum is 5 seconds, maximum is 30.
- `Ctrl-q`: quits
- `Ctrl-s`: take a snapshot for comparison, shown in the columns with `±`.

# Development Guide

## Quick start for devs

```
make setup
make test
```

launch local environment for testing the tools

```
make start
```

and tear it down

```
make stop
```
